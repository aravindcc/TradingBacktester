# You can set the Swift version to what you need for your app. Versions can be found here: https://hub.docker.com/_/swift
FROM swift:5.1.1

# For local build, add `--build-arg env=docker`
# In your application, you can use `Environment.custom(name: "docker")` to check if you're in this env
ARG env

RUN apt-get update
RUN apt-get install -y uuid-dev 
RUN apt-get install -y \
  libssl-dev zlib1g-dev \
  && rm -r /var/lib/apt/lists/*
WORKDIR /App
COPY ./TradingBacktester/Sources ./Sources
COPY ./TradingBacktester/Tests ./Tests
COPY ./TradingBacktester/Package.swift ./Package.swift
COPY ./Core/HarmonicCore /Core/HarmonicCore
COPY ./Core/TradingCore /Core/TradingCore
RUN cat Package.swift
RUN swift package resolve
RUN mkdir -p /build/lib && cp -R /usr/lib/swift/linux/*.so* /build/lib
RUN swift build -c release && mv `swift build -c release --show-bin-path` /build/bin