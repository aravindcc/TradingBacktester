import XCTest

import TradingBacktesterTests

var tests = [XCTestCaseEntry]()
tests += TradingBacktesterTests.allTests()
XCTMain(tests)
