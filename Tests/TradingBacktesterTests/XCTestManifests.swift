import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(TradingBacktesterTests.allTests),
    ]
}
#endif
