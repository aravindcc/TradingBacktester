//
//  Backtester.swift
//  HarmonicBacktester
//
//  Created by clar3 on 30/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import HarmonicCore
import TradingCore
import Async

extension TradeOrder {
    func update(from order: TradeOrder) {
        if !enteredPosition {
            entry = order.entry
            sl = bullish ? min(sl, order.sl) : max(sl, order.sl)
        } else {
            sl = order.sl
        }
        tps = order.tps
        patternEntries = order.patternEntries
    }
    var jsonRep: String {
        if  let data = try? JSONEncoder().encode(self),
            let str = String(data: data, encoding: .utf8) {
            return str
        }
        return patternKey
    }
}

extension PriceAction: Hashable {
    public static func == (lhs: PriceAction, rhs: PriceAction) -> Bool {
        return lhs.date == rhs.date
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(date)
    }
}

// backtest a currency and frame
class CurrencyFrameRunner: QuantTrader {
    
    enum NetworkError: Error {
        case Failure
    }
    
    func trade(order: TradeOrder) {
        for ord in deadOrders {
            if order == ord {
                return
            }
        }
        for (i, ord) in currentTrades.enumerated() {
            if order == ord {
                currentTrades[i].update(from: ord)
                return
            }
        }
        // add to queue: make sure pattern has settled
        currentTrades.append(order)
    }
    
    func remove(order: TradeOrder, finishedPrice: Double? = nil, endDate: Date? = nil) {
        let calcResult = { x in (x - order.entry) * (order.bullish ? 1 : -1) }
        let risk = calcResult(order.sl)
        let result = finishedPrice == nil ? nil : calcResult(finishedPrice!)
        let discovery = CachedDiscovery(instrument: order.pair, frame: order.frame, rawPatternData: order.patternEntries, patternKey: order.patternKey, entry: order.entry, currentSL: order.currentSL, tps: order.tps, sls: [order.sl], result: result, risk: risk, startDate: order.startDate, endDate: endDate)
        //delegate?.iterResults.append(discovery)
        currentTrades = currentTrades.filter { !($0 == order) }
        deadOrders.append(order)
    }
    
    func monitor(_ order: TradeOrder, currentPrice: ExchangeRate, date: Date?) {
        let ishQuantum = Int(floor((Date().timeIntervalSince1970 - order.startDate.timeIntervalSince1970) / order.frame.secondValue))
        if let waitingTakeTp = order.waitingTakeTp, ishQuantum != order.quantumFrom {
            order.quantumFrom = ishQuantum
            if order.laggingTakeTp == quant.LAG_TIME {
                order.laggingTakeTp = 0
                order.currentSL = Strategy.trailingStopLoss(currentAt: order.currentAt == nil ? order.entry : order.currentAt!, taketp: waitingTakeTp, currentSL: order.currentSL)
                order.currentAt = waitingTakeTp
                if quant.SHOULD_YOYO {
                    order.waitingTakeTp = nil
                }
            }
            order.laggingTakeTp += 1
        }
        
        let bullish = order.bullish
        let compare = { (price: Double, limit: Double) in
            return bullish ? price >= limit : price <= limit
        }
        // NOT REALY ASK AND BID THINK OF ASK HIGH AND LOW INSTEAD
        let cp = bullish ? currentPrice.ask : currentPrice.bid // usually
        let scp = !bullish ? currentPrice.ask : currentPrice.bid // usually
        if !order.enteredPosition {
            if let taketp = order.tps.first, compare(cp, taketp) {
                self.remove(order: order)
            }
            else if !compare(scp, order.bullish ? order.patternEntries.last!.low : order.patternEntries.last!.high) {
                self.remove(order: order)
            }
            else if currentPrice.ask > order.entry {
                order.enteredPosition = true
                // This should be taken care of by the entry order!
            }
        } else {
            if !compare(scp, order.currentSL) {
                // reached SL
                // This should be taken care of by the entry order!
                self.remove(order: order, finishedPrice: order.currentSL, endDate: date)
                return
            }
            if let taketp = order.tps.first, compare(cp, taketp) {
                if order.tps.count == 1 {
                    // reached final tp
                    // This should be taken care of by the entry order!
                    self.remove(order: order, finishedPrice: order.tps[0], endDate: date)
                    return
                }
                if order.currentAt == nil {
                    order.waitingTakeTp = order.tps.removeFirst()
                }
                else {
                    let _ = order.tps.removeFirst()
                    let bef = order.currentAt == nil ? order.entry : order.currentAt!
                    order.currentSL = Strategy.trailingStopLoss(currentAt: bef, taketp: taketp, currentSL: order.currentSL)
                    order.currentAt = taketp
                }
            }
        }
    }
    
    let quant: Quant
    private let fromDate: Date
    private weak var intervalDelegate: IntervalDelegate?
    private let broker: TradingClient
    private let pair: CurrencyPair
    private let frame: TimeFrame
    private var midData: [PriceAction] = []
    private var sellData: [PriceAction] = []
    private var buyData: [PriceAction] = []
    private let orderQueue = DispatchQueue(label: UUID().uuidString)
    private weak var delegate: BacktesterDelegate?
    var currentTrades: [TradeOrder] = []
    var deadOrders: [TradeOrder] = []
    
    init (_ broker: TradingClient,_ pair: CurrencyPair,_ frame: TimeFrame,_ fromDate: Date, intervalDelegate: IntervalDelegate, delegate: BacktesterDelegate?) throws {
        self.delegate = delegate
        self.fromDate = fromDate
        self.intervalDelegate = intervalDelegate
        self.broker = broker
        self.pair = pair
        self.frame = frame
        self.quant = Quant(false, broker)
        self.quant.trader = self
        try loadData().wait()
    }
    
    func loadData() -> Future<Void> {
        let midBars = try! broker.getBars(from: fromDate, for: pair, in: frame, ofType: .mid)
        let buyBars = try! broker.getBars(from: fromDate, for: pair, in: frame, ofType: .bid)
        let sellBars = try! broker.getBars(from: fromDate, for: pair, in: frame, ofType: .ask)
        return map(midBars, buyBars, sellBars) { (mid, buy, sell) in
            self.midData = mid.order()
            self.buyData = buy.order()
            self.sellData = sell.order()
        }
    }
    
    private func window<T>(_ input : [T], size : Int, execute : ([T], Float) -> Void ) {
        for i in 0...(input.count - size) {
            let window = Array(input[i..<(i + size)])
            execute(window, Float(i))
        }
    }
    
    func replay() {

        // clear
        quant.clearDiscoveries()
        currentTrades = []
        deadOrders = []
        
        // replay
        window(midData, size: frame == .daily ? 20 : 100) { [weak self] (window, i) in
            guard let self = self, let lastDate = window.last?.date.addingTimeInterval(frame.secondValue * 2 + 5) else { return }
            self.quant.analyse(data: window, for: self.pair, in: self.frame, goNext: false)
//            let shouldBuy = self.currentTrades.filter { $0.bullish }.count > 0
//            let shouldSell = self.currentTrades.filter { !$0.bullish }.count > 0
//            let processTrades = { (bullish: Bool) in
//                let priceType: PriceType = bullish ? .bid : .ask
//                if let intervalData = self.intervalDelegate?.getIntervalData(from: lastDate, till: nil, forType: priceType, andFrame: self.frame) {
//                    for action in intervalData {
//                        for trade in self.currentTrades {
//                            if trade.bullish == bullish {
//                                self.monitor(trade, currentPrice: ExchangeRate(bid: action.low, ask: action.high), date: action.date)
//                            }
//                        }
//                    }
//                }
//            }
//            if shouldBuy { processTrades(true) }
//            if shouldSell { processTrades(false) }
        }
    }
}
