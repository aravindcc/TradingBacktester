//
//  QuantInput.swift
//  HarmonicBacktester
//
//  Created by clar3 on 07/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import HarmonicCore

enum SlippageType: Int, Codable {
    case volatile = 0
    case normal = 1
}

extension HarmonicPattern: Equatable, Hashable {
    public static func ==(lhs: HarmonicPattern, rhs: HarmonicPattern) -> Bool {
        return lhs.fullName == rhs.fullName
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(fullName)
    }
}

extension HarmonicPattern: CustomStringConvertible {
    public var description: String {
        return fullName
    }
}

class QuantInput: Hashable, Codable, CustomStringConvertible {
    static func ==(lhs: QuantInput, rhs: QuantInput) -> Bool {
        return  lhs.bullishWeight == rhs.bullishWeight &&
                lhs.bearishWeight == rhs.bearishWeight &&
                lhs.errorShortFrame == rhs.errorShortFrame &&
                lhs.slippageAdjust == rhs.slippageAdjust &&
                lhs.errorMediumFrame == rhs.errorMediumFrame &&
                lhs.lagTime == rhs.lagTime &&
                lhs.shouldYOYO == rhs.shouldYOYO &&
                lhs.harmonicOrder == rhs.harmonicOrder &&
                lhs.windowSize == rhs.windowSize
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(bullishWeight)
        hasher.combine(bearishWeight)
        hasher.combine(errorShortFrame)
        hasher.combine(errorMediumFrame)
        hasher.combine(slippageAdjust)
        hasher.combine(lagTime)
        hasher.combine(shouldYOYO)
        hasher.combine(harmonicOrder)
        hasher.combine(windowSize)
    }
    
    var description: String {
        return "(BULLISH: \(bullishWeight), BEARISH \(bearishWeight), SHORT RATE \(errorShortFrame), MEDIUM RATE \(errorMediumFrame), SLIPPAGE \(slippageAdjust), LAG \(lagTime), YOYO \(shouldYOYO)), WINDOW \(windowSize)"
    }
    
    var bullishWeight: Double
    var bearishWeight: Double
    var errorShortFrame: Double
    var errorMediumFrame: Double
    var slippageAdjust: Double
    var lagTime: Int
    var shouldYOYO: Bool
    var slippageType: SlippageType = .normal
    var harmonicOrder: [HarmonicPattern]
    var windowSize: Int
    
    init() {
        self.bullishWeight = 0
        self.windowSize = 15
        self.bearishWeight = 0
        self.errorShortFrame = 0.03
        self.errorMediumFrame = 0.05
        self.slippageAdjust = 1
        self.lagTime = 0
        self.shouldYOYO = false
        self.harmonicOrder = HarmonicPatterns.getAllPatterns()
    }
}
