//
//  Searcher.swift
//  HarmonicBacktester
//
//  Created by clar3 on 07/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import TradingCore


class Searcher {

    let space: [Any]
    private var position: Int = 0
    private var children: [Searcher] = []
    private let assigner: (QuantInput, Any) -> Void
    init(space: [Any], state: @escaping (QuantInput, Any) -> Void) {
        self.space = space
        self.assigner = state
    }
    
    func step() -> Bool {
        var childrenEnded = true
        for child in children {
            childrenEnded = child.step() && childrenEnded
        }
        
        if childrenEnded {
            if position < (space.count - 1) {
                position += 1
                for child in children {
                    child.position = 0
                }
                return false
            }
            return true
        }
        return false
    }
    
    var percentage: Double {
        if children.count > 0 {
            var percentage: Double?
            for child in children {
                percentage = percentage == nil ? child.percentage : min(child.percentage, percentage!)
            }
            return (Double(position) + percentage!) / Double(space.count)
        }
        return (Double(position) + 1) / Double(space.count)
    }

    func add(child: Searcher) {
        children.append(child)
    }
    
    func add(children newChildren: [Searcher]) {
        children.append(contentsOf: newChildren)
    }
    
    func propogate(into input: QuantInput) {
        children.forEach { $0.propogate(into: input) }
        assigner(input, space[position])
    }
    
    func maxIter() -> Int {
        return space.count * (children.map({ $0.maxIter() }).max() ?? 1)
    }
}

struct ResultCapture: Codable {
    init(totalResultMedium: Double, totalResultShort: Double, rate: Double, wins: Double, losses: Double, missed: Int, avgwin: Double, avgloss: Double, pair: String) {
        self.totalResultMedium = totalResultMedium.isNaN ? -1 : totalResultMedium
        self.totalResultShort = totalResultShort.isNaN ? -1 : totalResultShort
        self.rate = rate.isNaN ? -1 : rate
        self.wins = wins.isNaN ? -1 : wins
        self.losses = losses.isNaN ? -1 : losses
        self.missed = missed
        self.avgwin = avgwin.isNaN ? -1 : avgwin
        self.avgloss = avgloss.isNaN ? -1 : avgloss
        self.pair = pair
    }
    
    let totalResultMedium: Double
    let totalResultShort: Double
    let rate: Double
    let wins: Double
    let losses: Double
    let missed: Int
    let avgwin: Double
    let avgloss: Double
    let pair: String
}
