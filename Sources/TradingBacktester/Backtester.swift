//
//  Backtester.swift
//  HarmonicBacktester
//
//  Created by clar3 on 30/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import TradingCore
import HarmonicCore

extension Quant {
    func discoverResults(pair: String = "all") -> ResultCapture {
        var totalResultMedium = 0.0
        var totalResultShort = 0.0
        var wins = 0.0
        var losses = 0.0
        var missed = 0
        var totalWin = 0.0
        var totalLoss = 0.0
        var results: [CachedDiscovery] = []
        for (key, val) in discoveryStore {
            var res = 0.0
            for v in val {
                results.append(v)
                var ov = v.result ?? 0.0
                if key.contains(string: "JPY") {
                    ov /= 100
                }
                if ov == 0 {
                    missed += 1
                } else if ov > 0 {
                    wins += 1
                    totalWin += ov
                }  else {
                    losses += 1
                    totalLoss += ov
                }
                
                res += ov
            }
            if key.contains(string: "DAILY") {
                totalResultMedium += res
            } else {
                totalResultShort += res
            }
        }
        let rate = wins / (wins + losses)
        let avgwin = totalWin / wins
        let avgloss = totalLoss / losses
        results.sort { (one, two) -> Bool in
            let oneDate = one.startDate ?? .distantFuture
            let twoDate = two.startDate ?? .distantFuture
            return oneDate.compare(twoDate) == .orderedAscending
        }
        results.forEach { (v) in
            print("\(v.instrument);\(v.startDate ?? .distantFuture);(\(v.entry));(\(v.result ?? 0.0))")
        }
        return ResultCapture(totalResultMedium: totalResultMedium, totalResultShort: totalResultShort, rate: rate,
                             wins: wins, losses: losses, missed: missed, avgwin: avgwin, avgloss: avgloss, pair: pair)
    }
    
    func apply(inputs input: QuantInput) {
        HarmonicAnalyser.bullishWeight = input.bullishWeight
        HarmonicAnalyser.bearishWeight = input.bearishWeight
        HarmonicAnalyser.errorAllowed  = { (frame: TimeFrame, bullish: Bool) -> Double in
            var rawVal: Double!
            switch frame {
            case .sixtyMinute, .thirtyMinute, .fifteenMinute:
                rawVal = input.errorShortFrame
            case .daily, .weekly:
                rawVal = input.errorMediumFrame
            default:
                rawVal = 0.05
            }
            return bullish ? rawVal * HarmonicAnalyser.bullishWeight : rawVal * HarmonicAnalyser.bearishWeight
        }
        slippageAdjust = { pair in input.slippageAdjust * (input.slippageType == .normal ? pair.slippage : pair.volatile_slippage ) }
        SHOULD_YOYO = input.shouldYOYO
        LAG_TIME = input.lagTime
        harmonicOrder = input.harmonicOrder
        windowSize = input.windowSize
    }
}

extension Dictionary where Key == QuantInput, Value == ResultCapture {
    
    mutating func shuntResultsToFile(pair: String? = nil, log: ((String) -> Void)? = nil) {
        guard let val = try? JSONEncoder().encode(self) else { return }
        let root_path = Conversion.rootDataDirectory()
        let filename = pair == nil ? URL(fileURLWithPath: "\(root_path)insight/results.txt") :
            URL(fileURLWithPath: "\(root_path)insight/results-\(pair!).txt")
        log?("SHUNTING TO \(filename)")
        do {
            try val.write(to: filename)
        } catch {
            log?("FAILED TO WRITE FILE \(error)")
        }
        self = [:]
    }
}

protocol BacktesterDelegate: AnyObject {
    func finished() -> Void
    func log(_ :String) -> Void
    var iterResults: [CachedDiscovery] { get set }
}

class Backtester {
    
    let pairs: [CurrencyPair]
    var runners : [CurrencyRunner] = []
    var results: [QuantInput: ResultCapture] = [:]
    var currentInput: QuantInput?
    var rootSearcher: Searcher?
    var startTime: Date?
    var iterations: Int = 0
    var avgTime: Double = 0
    let shouldExplore = true
    weak var delegate: BacktesterDelegate?
    var iterationCounter = 0
    let queue: DispatchQueue
    var chunkCount = 0
    
    init(broker: TradingClient, delegate: BacktesterDelegate, pairs: [CurrencyPair], frames: [TimeFrame], fromDate date: Date,_ queue: DispatchQueue) throws {
        self.queue = queue
        self.delegate = delegate
        self.pairs = pairs
        for pair in pairs {
            let runner = try CurrencyRunner(broker, pair, frames: frames, fromDate: date, delegate: delegate)
            log("LOADED \(pair)")
            runners.append(runner)
        }
    }
    
    func log(_ input: String) {
        delegate?.log(input)
    }
    
    func begin() {
        log("TESTING IN CHUNKS")
        if shouldExplore {
            setSearchSpace()
            if let search = rootSearcher {
                let input = QuantInput()
                search.propogate(into: input)
                self.run(input: input)
            }
        } else {
            while true {
                let input = getConfigArray()
                run(input: input)
            }
        }
    }
    
    func toInput(_ num: [Double]) -> QuantInput {
        let input = QuantInput()
        input.bearishWeight = num[0]
        input.bullishWeight = num[1]
        input.errorMediumFrame = num[2]
        input.errorShortFrame = num[3]
        input.lagTime = Int(num[4])
        input.shouldYOYO = num[5] != 0
        input.slippageAdjust = num[6]
        input.slippageType = .normal
        return input
    }
    
    func getConfigArray() -> QuantInput {
        log("ENTER NEW CONFIG")
        let input_str = readLine()
        var num = [1.08, 0.920, 0.25, 0.17, 0, 1, 1]
        if let input_num = input_str?.split(separator: "\t").compactMap(Double.init), input_num.count == 7 {
            num = input_num
        } else if let input_num = input_str?.split(separator: " ").compactMap(Double.init), input_num.count == 7 {
            num = input_num
        }
        return toInput(num)
    }
    
    func permute<C: Collection>(_ items: C) -> [[C.Iterator.Element]] {
        var scratch = Array(items) // This is a scratch space for Heap's algorithm
        var result: [[C.Iterator.Element]] = [] // This will accumulate our result
        
        // Heap's algorithm
        func heap(_ n: Int) {
            if n == 1 {
                result.append(scratch)
                return
            }
            
            for i in 0..<n-1 {
                heap(n-1)
                let j = (n%2 == 1) ? 0 : i
                scratch.swapAt(j, n-1)
            }
            heap(n-1)
        }
        
        // Let's get started
        heap(scratch.count)
        
        // And return the result we built up
        return result
    }
    
    
    func setSearchSpace() {
        let trendSearch = Searcher(space: Array(stride(from: -0.3, through: 0.3, by: 0.01))) { (input, value) in
            guard let value = value as? Double else { return }
            input.bullishWeight = 1.0 + value
            input.bearishWeight = 1.0 - value
        }
        let harmonicSearch = Searcher(space: permute(0...3)) { (input, value) in
            guard let value = value as? [Int] else { return }
            let base = HarmonicPatterns.getAllPatterns()
            var permuted = HarmonicPatterns.getAllPatterns()
            for i in [0, 1] {
                for j in 0...3 {
                    let k = 4*i + j
                    let to = 4*i + value[j]
                    permuted[k] = base[to]
                }
            }
            input.harmonicOrder = permuted
        }
        let errorShortSearch =  Searcher(space: Array(stride(from: 0.03, through: 0.08, by: 0.02))) { (input, value) in
            guard let value = value as? Double else { return }
            input.errorShortFrame = value
        }
        let windowSearch = Searcher(space: Array(stride(from: 10, through: 20, by: 2))) { (input, value) in
            guard let value = value as? Int else { return }
            input.windowSize = value
        }
        let lagTimeSearch = Searcher(space: Array(0...2)) { (input, value) in
            guard let value = value as? Int else { return }
            input.lagTime = value
        }
        let yoyoSearch = Searcher(space: [false]) { (input, value) in
            guard let value = value as? Bool else { return }
            input.shouldYOYO = value
        }
        
        rootSearcher = yoyoSearch
        yoyoSearch.add(child: lagTimeSearch)
        lagTimeSearch.add(child: trendSearch)
        trendSearch.add(child: harmonicSearch)
        harmonicSearch.add(child: windowSearch)
        windowSearch.add(child: errorShortSearch)
    }
    
    func iterate() -> QuantInput {
        let _ = rootSearcher?.step()
        let input = QuantInput()
        rootSearcher?.propogate(into: input)
        return input
    }
    
    var metricHolder: [String: (Double, Double)] = [:]
    @discardableResult
    func measure<A>(_ name: String = "", _ block: () throws -> A) rethrows -> A {
        let startTime = Foundation.ProcessInfo.processInfo.systemUptime
        let result = try block()
        let timeElapsed = Foundation.ProcessInfo.processInfo.systemUptime - startTime
        let newTotal = (metricHolder[name]?.0 ?? 0) + timeElapsed
        let newCount = (metricHolder[name]?.1 ?? 0) + 1
        metricHolder[name] = (newTotal, newCount)
        return result
    }
    
    private func run(input: QuantInput) {
        measure("explore") { () -> Void in
            //let pair = pairs.count == 1 ? "\(pairs[0])" : "all"
            //delegate?.iterResults = []
            runners.forEach { $0.input = input; $0.replay() }
            //return delegate?.iterResults.discoverResults(printEntry: false, pair: pair)
        }
        
//        guard let capture = explore else {
//            self.log("FAILED ITERATION ERROR OCCURED")
//            return
//        }
        
        if shouldExplore {
           // self.results[input] = capture
            
            if self.results.count > 25000 {
                results.shuntResultsToFile(pair: "\(pairs[0])-\(chunkCount)", log: log)
                chunkCount += 1
            }
            
            let percent = (rootSearcher?.percentage ?? 0)
            guard percent < 1.0 else {
                if pairs.count == 1 {
                    log("FINISHED LOCAL")
                    results.shuntResultsToFile(pair: "\(pairs[0])-\(chunkCount)", log: log)
                } else {
                    results.shuntResultsToFile()
                    Conversion.run()
                }
                queue.asyncAfter(deadline: .now() + 0.0001) { [weak self] in
                    guard let self = self else { return }
                    self.log("CALLING DELEGATE")
                    self.delegate?.finished()
                }
                return
            }
            iterationCounter += 1
            if iterationCounter == 1 {
                log("RAN FIRST ITERATION IN \(metricHolder["explore"]!.0.format4())")
            }
            if iterationCounter%500 == 0 {
                let avgTime = metricHolder["explore"]!.0 / metricHolder["explore"]!.1
                let secondsLeft = (1 - percent) * Double(rootSearcher!.maxIter()) * avgTime
                let qor = { (a: Double, b: Double) -> (Int, Int) in
                    let q = floor(a / b)
                    return (Int(q), Int(a - q*b))
                }
                let (hrs, r) = qor(secondsLeft, 3600)
                let (mins, secs) = qor(Double(r), 60)
                log("EVOLVING: \((percent * 100).format4()); AVG_TIME: \(avgTime)) TIME_LEFT: \(hrs)h \(mins)m \(secs)s")
            }
            
            queue.asyncAfter(deadline: .now() + 0.0001) { [weak self] in
                guard let next_iteration = self?.iterate() else { return }
                self?.run(input: next_iteration)
            }
        }
//        else {
//            print(capture)
//        }
    }
    
    func getBestWinRate() -> (QuantInput, ResultCapture) {
        var best = -100.0
        var return_input: (QuantInput, ResultCapture)!
        for (input, capture) in results {
            if capture.rate > best {
                best = capture.rate
                return_input = (input, capture)
            }
        }
        return return_input
    }
    
    func getBestResult() -> (QuantInput, ResultCapture) {
        var best = -100.0
        var return_input: (QuantInput, ResultCapture)!
        for (input, capture) in results {
            if capture.totalResultShort > best {
                best = capture.totalResultShort
                return_input = (input, capture)
            }
        }
        return return_input
    }
    
    func showResult(_ res: (QuantInput, ResultCapture)) -> String {
        return "\(res.0); \(res.1.rate.format4()); \(res.1.totalResultShort.format4())"
    }
    
    deinit {
        print("LEAVING THE WORLD")
    }
}

