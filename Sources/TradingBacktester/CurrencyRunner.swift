//
//  CurrencyRunner.swift
//  HarmonicBacktester
//
//  Created by clar3 on 31/05/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import HarmonicCore
import TradingCore
import Async

protocol IntervalDelegate: AnyObject {
    func getIntervalData(from startDate: Date, till endDate: Date?, forType type: PriceType, andFrame frame: TimeFrame) -> [PriceAction]
}

extension Array where Element == PriceAction {
    func order() -> [PriceAction] {
        self.sorted { (one, two) -> Bool in
            return one.date.compare(two.date) == .orderedAscending
        }
    }
}

extension Array where Element == CachedDiscovery {
    mutating func discoverResults(printEntry: Bool = true, pair: String = "all") -> ResultCapture {
        var total = 0.0
        var wins = 0.0
        var losses = 0.0
        var missed = 0
        var totalWin = 0.0
        var totalLoss = 0.0
        for v in self {
            var ov = v.result ?? 0.0
            if v.instrument.rawValue.contains(string: "JPY") {
                ov /= 100
            }
            if ov == 0 {
                missed += 1
            } else if ov > 0 {
                wins += 1
                totalWin += ov
            }  else {
                losses += 1
                totalLoss += ov
            }
            total += ov
        }
        let rate = wins / (wins + losses)
        let avgwin = totalWin / wins
        let avgloss = totalLoss / losses
        
        if printEntry {
            self.sort { (one, two) -> Bool in
                let oneDate = one.startDate ?? .distantFuture
                let twoDate = two.startDate ?? .distantFuture
                return oneDate.compare(twoDate) == .orderedAscending
            }
            self.forEach { (v) in
                print("(\(v.entry));")
            }
        }
        return ResultCapture(totalResultMedium: 0.0, totalResultShort: total, rate: rate, wins: wins,
                             losses: losses, missed: missed, avgwin: avgwin, avgloss: avgloss, pair: pair)
    }
}

class CurrencyRunner: IntervalDelegate {
    private let fromDate: Date
    private let broker: TradingClient
    private let pair: CurrencyPair
    private let frames: [TimeFrame]
    private var runners: [CurrencyFrameRunner] = []
    private var intervalBuyData: [Date: PriceAction] = [:]
    private var intervalSellData: [Date: PriceAction] = [:]
    private weak var delegate: BacktesterDelegate?
    var input: QuantInput? {
        didSet {
            if input != nil {
                for runner in runners {
                    runner.quant.apply(inputs: input!)
                }
            }
        }
    }
    
    init(_ broker: TradingClient,_ pair: CurrencyPair, frames: [TimeFrame], fromDate: Date, delegate: BacktesterDelegate?) throws {
        self.broker = broker
        self.delegate = delegate
        self.fromDate = fromDate
        self.pair = pair
        self.frames = frames
        try loadIntervalData().wait()
        for frame in frames {
            let runner = try CurrencyFrameRunner(broker, pair, frame, fromDate, intervalDelegate: self, delegate: delegate)
            runners.append(runner)
        }
    }
    
    func replay() {
        runners.forEach { $0.replay() }
    }
    
    func loadIntervalData() -> Future<Void> {
        let buyInterval = fillIntervalData(buying: true)
        let sellInterval = fillIntervalData(buying: false)
        return map(buyInterval, sellInterval, {_, _ in () })
    }
    
    private func fillIntervalData(buying: Bool, endDate: Date? = nil, held: [PriceAction] = [], promise: EventLoopPromise<Void>? = nil) -> Future<Void> {
        let promise = promise ?? broker.worker.eventLoop.newPromise(Void.self)
        let date = endDate ?? self.fromDate
        let bars = try! broker.getBars(from: date, for: pair, in: .fiveMinute, ofType: .bid)
        bars.do { (prices) in
            if prices.count == 0 {
                for found in held.sorted(by: { a, b in a.date.compare(b.date) == .orderedAscending }) {
                    if buying {
                        self.intervalBuyData[found.date] = found
                    } else {
                        self.intervalSellData[found.date] = found
                    }
                }
                promise.succeed()
            } else {
                let new = prices.last!.date.addingTimeInterval(300)
                let newHeld = prices + held
                let _ = self.fillIntervalData(buying: buying, endDate: new, held: newHeld, promise: promise)
            }
        }.catch { error in
            promise.fail(error: error)
        }
        return promise.futureResult
    }
    
    func getIntervalData(from startDate: Date, till endDate: Date? = nil, forType type: PriceType, andFrame frame: TimeFrame) -> [PriceAction] {
        let endDate = endDate ?? startDate.addingTimeInterval(frame.secondValue * 2 + 5)
        let heldData = type == .bid ? self.intervalBuyData : self.intervalSellData
        var held: [PriceAction] = []
        var currDate = startDate
        while currDate <= endDate {
            if let found = heldData[currDate] {
                held.append(found)
            }
            currDate = currDate.addingTimeInterval(60*5)
        }
        return held
    }
}
