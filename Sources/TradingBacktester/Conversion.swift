//
//  Conversion.swift
//  HarmonicBacktester
//
//  Created by clar3 on 01/06/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import HarmonicCore

enum ConversionError: Error {
    case dataCorruption
}

extension Array where Element: Any {
    func toCSVLine() -> String {
        return String(reduce("", { $0 + ",\($1)"}).dropFirst())
    }
    func toCSVTable() -> String {
        return String(reduce("", { $0 + "\n\($1)"}).dropFirst())
    }
}

public class Conversion {
    public static func rootDataDirectory() -> String {
        return ProcessInfo.processInfo.environment["FX_DOCKER"] != nil ? "/home/aravind_dharmalingam/CloudTester/" : "/private/var/clar3/Desktop/Projects/Forex/"
    }
    
    public static func run(ext: String? = nil, log: ((String) -> Void)? = nil) {
        let root = rootDataDirectory()
        let filename = ext == nil ? URL(fileURLWithPath: "\(root)insight/results.txt")
                                : URL(fileURLWithPath: "\(root)insight/results-\(ext!).txt")
        let outputFile = ext == nil ? URL(fileURLWithPath: "\(root)insight/results.csv")
                                    : URL(fileURLWithPath: "\(root)insight/results-\(ext!).csv")

        do {
            log?("BEGINNING CONVERSION")
            let loaded = try JSONSerialization.jsonObject(with: try Data(contentsOf: filename)) as! [[String: Any]]
            let results = stride(from: 0, to: loaded.endIndex, by: 2).map {
                (loaded[$0], $0 < loaded.index(before: loaded.endIndex) ? loaded[$0.advanced(by: 1)] : nil)
            }
            var header: [String] = []
            let table = results.compactMap { (inputs, outputs) -> String? in
                do {
                    guard let outputs = outputs
                    else { throw ConversionError.dataCorruption }
                    if header.count == 0 {
                        header = inputs.keys.sorted() + outputs.keys.sorted()
                    }
                    let allValues = header.compactMap { (key: String) -> Any? in
                        guard let obj = inputs[key] ?? outputs[key] else { return nil }
                        if let patterns = obj as? [[String: Any]] {
                            return patterns.compactMap({
                                if let name = $0["name"] as? String, ($0["direction"] as? Int) == 0 {
                                    return name
                                }
                                return nil
                            }).joined(separator: "-")
                        }
                        return obj
                    }
                    return allValues.toCSVLine()
                } catch {
                    return nil
                }
            }
            let finalString = [header.toCSVLine()] + table
            try finalString.toCSVTable().write(to: outputFile, atomically: true, encoding: .utf8)
            log?("FINISHED CONVERSION")
        } catch {
            log?("FAILED TO CONVERT: \(error)")
        }
    }
}

