//
//  File.swift
//  
//
//  Created by clar3 on 03/06/2020.
//

import Foundation
import TradingCore
import HarmonicCore

public extension String {
    func toDate(withFormat format: String = "dd/MM/yy")-> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Tehran")
        dateFormatter.locale = Locale(identifier: "fa-IR")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)

        return date
    }
}

public class Experiment: BacktesterDelegate {
    

    public var frames: [TimeFrame] = [.thirtyMinute, .sixtyMinute]
    public var date = "03/02/20".toDate()!
    public var pairs = CurrencyPair.allPairs
    public var onFinish: (() -> Void)?
    public var log: ((String) -> Void)?
    
    var iterResults: [CachedDiscovery] = []
    private var currentTester: Backtester?
    private var broker: TradingClient!
    private var queue: DispatchQueue!
    var i = -1
    
    public init() throws {
        while broker == nil {
            broker = try? TradingClient()
            sleep(5)
        }
        print("TOKEN: \(broker.sessionToken ?? "NONE")")
        queue = DispatchQueue.global(qos: .background)
    }
    
    func finished() {
        currentTester = nil
        if i >= 0 {
            log?("FINISHED \(pairs[i]), \(i + 1)/\(pairs.count)")
        }
        i += 1
        if i < pairs.count {
            let pair = pairs[i]
            do {
                let new = try Backtester(broker: broker, delegate: self, pairs: [pair], frames: frames, fromDate: date, queue)
                currentTester = new
                new.begin()
            } catch {
                finished()
            }
        } else {
            onFinish?()
        }
    }
    
    func log(_ input: String) {
        log?(input)
    }
    
    public func run() {
        i = -1
        queue.async {
            self.finished()
        }
    }
}
