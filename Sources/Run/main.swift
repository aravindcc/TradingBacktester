//
//  File.swift
//  
//
//  Created by clar3 on 03/06/2020.
//

import Foundation
import TradingBacktester
import HarmonicCore
import TradingCore

internal struct FileHandleOutputStream: TextOutputStream {
    private let fileHandle: FileHandle
    let encoding: String.Encoding

    init(_ fileHandle: FileHandle, encoding: String.Encoding = .utf8) {
        self.fileHandle = fileHandle
        self.encoding = encoding
    }

    mutating func write(_ string: String) {
        if let data = string.data(using: encoding) {
            fileHandle.write(data)
        }
    }
}
internal var STDERR = FileHandleOutputStream(.standardError)
internal var STDOUT = FileHandleOutputStream(.standardOutput)

let sema = DispatchSemaphore(value: 0)
print("STARTING", to: &STDOUT)
print("ROOT DATA DIRECTORY IS \(Conversion.rootDataDirectory())", to: &STDOUT)


let experiment = try Experiment()
experiment.frames = [.thirtyMinute, .sixtyMinute]
experiment.date = "01/01/20".toDate()!
experiment.pairs = CurrencyPair.allPairs
//pairs = [.NZD_USD, .GBP_USD, .USD_CHF, .EUR_CHF, .EUR_JPY, .AUD_USD, .GBP_CAD]
//pairs = [.AUD_USD]

HarmonicAnalyser.extremaAnchor = {(_ bar: PriceAction,_ isLow: Bool) -> Double in
    return isLow ? bar.low : bar.high
}
experiment.onFinish = {
    sema.signal()
}
experiment.log = {
    print($0, to: &STDOUT)
}
experiment.run()
sema.wait()

